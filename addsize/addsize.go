package main

import (
	"fmt"
	"os"
	"path/filepath"

	"git.gitorious.org/trantor/trantor.git/database"
)

const (
	DB_IP   = "127.0.0.1"
	DB_NAME = "trantor"
	STORE   = "store"
	EPUB    = "book.epub"
)

func main() {
	db := database.Init(DB_IP, DB_NAME)
	defer db.Close()

	walk := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Println("There is an error with ", path, ": ", err)
			return nil
		}

		if filepath.Base(path) != EPUB {
			return nil
		}

		id := filepath.Base(filepath.Dir(path))
		fmt.Println("====== ", id, " ======")

		size := info.Size()
		err = db.UpdateBook(id, map[string]interface{}{"filesize": size})
		if err != nil {
			fmt.Println("Error updateing db: ", err)
		}
		return nil
	}

	filepath.Walk(STORE, walk)
}
