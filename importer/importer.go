package main

import log "github.com/cihub/seelog"

import (
	"os"

	"git.gitorious.org/go-pkg/epubgo.git"
	"git.gitorious.org/trantor/trantor.git/database"
	"git.gitorious.org/trantor/trantor.git/storage"
)

func main() {
	db := database.Init(DB_IP, DB_NAME)
	defer db.Close()

	store, err := storage.Init(STORE_PATH)
	if err != nil {
		log.Critical(err)
		os.Exit(1)
	}

	for _, file := range os.Args[1:len(os.Args)] {
		uploadEpub(file, db, store)
	}
}

func uploadEpub(filename string, db *database.DB, store *storage.Store) {
	epub, err := epubgo.Open(filename)
	if err != nil {
		log.Error("Not valid epub '", filename, "': ", err)
		return
	}
	defer epub.Close()

	book, id := parseFile(epub, store)
	title, _ := book["title"].(string)
	_, numTitleFound, _ := db.GetBooks("title:"+title, 0, 0)
	if numTitleFound == 0 {
		book["active"] = true
	}

	file, _ := os.Open(filename)
	defer file.Close()
	size, err := store.Store(id, file, EPUB_FILE)
	if err != nil {
		log.Error("Error storing book (", title, "): ", err)
		return
	}

	book["filesize"] = size
	err = db.AddBook(book)
	if err != nil {
		log.Error("Error storing metadata (", title, "): ", err)
		return
	}
	log.Info("File uploaded: ", filename)
}
